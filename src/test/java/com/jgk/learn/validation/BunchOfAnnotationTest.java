package com.jgk.learn.validation;

import org.junit.Before;
import org.junit.Test;

/*
 * 
 * https://beanvalidation.org/2.0/spec/#builtinconstraints
 */
public class BunchOfAnnotationTest extends AbstractValidationTestFixture {

	private BunchOfAnnotationObject boa;

	@Before
	public void setup() {
		super.setup();
		boa = new BunchOfAnnotationObject().setAnEmail("some@comp.com").setShouldBeNull(null)
				.setShouldBeFalse(false)
				.setShouldBeTrue(true)
				.setMinMax(BunchOfAnnotationObject.MIN)
				.setShouldNotBeNull("not-null");
	}

	@Test
	public void testMinMax() {
		assertNoValidationErrors(validator.validate(boa.setMinMax(BunchOfAnnotationObject.MIN)));
		assertNoValidationErrors(validator.validate(boa.setMinMax(BunchOfAnnotationObject.MAX)));
		assertHasValidationErrors(validator.validate(boa.setMinMax(BunchOfAnnotationObject.MIN-1)));
		assertHasValidationErrors(validator.validate(boa.setMinMax(BunchOfAnnotationObject.MAX+1)));
	}
	@Test
	public void testAssertFalse() {
		assertNoValidationErrors(validator.validate(boa.setShouldBeFalse(false)));
		assertHasValidationErrors(validator.validate(boa.setShouldBeFalse(true)));
	}

	@Test
	public void testAssertTrue() {
		assertNoValidationErrors(validator.validate(boa.setShouldBeTrue(true)));
		assertHasValidationErrors(validator.validate(boa.setShouldBeTrue(false)));
	}

	@Test
	public void testEmail() {
		assertNoValidationErrors(validator.validate(boa.setAnEmail("wilma@fred.com")));
		assertHasValidationErrors(validator.validate(boa.setAnEmail("FRED")));
	}

	@Test
	public void testNull() {
		assertHasValidationErrors(validator.validate(boa.setShouldBeNull("not a null")));
		assertNoValidationErrors(validator.validate(boa.setShouldBeNull(null)));
	}

	@Test
	public void testNotNull() {
		assertNoValidationErrors(validator.validate(boa.setShouldNotBeNull("not a null")));
		assertHasValidationErrors(validator.validate(boa.setShouldNotBeNull(null)));
	}

}
