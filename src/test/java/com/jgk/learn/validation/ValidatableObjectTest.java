package com.jgk.learn.validation;

import static org.junit.Assert.assertEquals;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Test;

public class ValidatableObjectTest {

	@Test
	public void test() {
		ValidatableObject validatableObject = new ValidatableObject(null);
        // validate the input
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<ValidatableObject>> violations = validator.validate(validatableObject);
        for (ConstraintViolation<ValidatableObject> constraintViolation : violations) {
			System.out.println(constraintViolation);
		}
        assertEquals(1,violations.size());

	}

}
