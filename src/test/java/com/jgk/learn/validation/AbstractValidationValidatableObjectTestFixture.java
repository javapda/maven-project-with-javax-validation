package com.jgk.learn.validation;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;

public abstract class AbstractValidationValidatableObjectTestFixture<ValidatableObjectType>
		extends AbstractValidationTestFixture {

	@Before
	public void setup() {
		super.setup();
		assertNotNull(validatableObject());
	}

	protected abstract ValidatableObjectType validatableObject();

}
