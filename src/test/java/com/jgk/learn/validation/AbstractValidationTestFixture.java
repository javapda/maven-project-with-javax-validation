package com.jgk.learn.validation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Before;

public abstract class AbstractValidationTestFixture {
	protected Validator validator;

	@Before
	public void setup() {

		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		assertNotNull(factory);
		validator = factory.getValidator();
		assertNotNull(validator);

	}

	public void assertNoValidationErrors(Collection<?> violations) {
		assertTrue(violations.isEmpty());
	}

	public void assertHasValidationErrors(Collection<?> violations) {
		assertFalse(violations.isEmpty());
	}

}
