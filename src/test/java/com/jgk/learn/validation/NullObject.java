package com.jgk.learn.validation;

import javax.validation.constraints.Null;

public class NullObject {
	@Null
	private String name;
	
	public NullObject() {
		
	}

	public NullObject setName(String name) {
		this.name = name;
		return this;
	}

}