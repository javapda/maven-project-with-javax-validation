package com.jgk.learn.validation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.Set;

import javax.validation.ConstraintViolation;

import org.junit.Test;

public class NullAnnotationTest extends AbstractValidationValidatableObjectTestFixture<NullObject> {
	
	@Test
	public void test() {
		validator.validate(validatableObject());
	}

	@Test
	public void testWithNonNull() {
		Set<ConstraintViolation<Object>> c = validator.validate(validatableObject().setName("JED"));
		assertFalse(c.isEmpty());
		assertEquals(1, c.size());

	}

	@Override
	protected NullObject validatableObject() {
		return new NullObject().setName(null);
	}

}
