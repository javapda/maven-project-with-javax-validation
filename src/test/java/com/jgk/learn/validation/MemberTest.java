package com.jgk.learn.validation;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Test;

public class MemberTest {

	@Test
	public void test() {
		new Member().getFirstName();
	}
	
	@Test
    public void testMemberWithNoValues() {
        Member member = new Member();
        member.setEmailAddress("jed@clampett.com");
//        member.setLastName("LastName");
        member.setGender(Gender.MALE);
        member.setDateOfBirth(new Date());
        member.setFirstName("FirstName");
        

        // validate the input
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<Member>> violations = validator.validate(member);
        for (ConstraintViolation<Member> constraintViolation : violations) {
			System.out.println(constraintViolation);
			System.out.println(constraintViolation.getClass());
		}
        assertEquals(2,violations.size());
    }

}
