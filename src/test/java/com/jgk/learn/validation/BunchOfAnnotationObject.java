package com.jgk.learn.validation;

import javax.validation.constraints.AssertFalse;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

/**
 * https://beanvalidation.org/2.0/spec/#builtinconstraints
 * @author jkroub
 *
 */
public class BunchOfAnnotationObject {
	
	public static final int MIN = 100;
	public static final int MAX = 100;
	
	
	@Min(value=MIN)
	@Max(value=MAX)
	private int minMax;

	@Email
	private String anEmail;

	@Null
	private String shouldBeNull;
	
	@NotNull
	private String shouldNotBeNull;
	
	@AssertTrue
	private boolean shouldBeTrue;

	@AssertFalse
	private boolean shouldBeFalse;

	public String getAnEmail() {
		return anEmail;
	}

	public BunchOfAnnotationObject setAnEmail(String anEmail) {
		this.anEmail = anEmail;
		return this;
	}

	public String getShouldBeNull() {
		return shouldBeNull;
	}

	public BunchOfAnnotationObject setShouldBeNull(String shouldBeNull) {
		this.shouldBeNull = shouldBeNull;
		return this;
	}

	public String getShouldNotBeNull() {
		return shouldNotBeNull;
	}

	public BunchOfAnnotationObject setShouldNotBeNull(String shouldNotBeNull) {
		this.shouldNotBeNull = shouldNotBeNull;
		return this;
	}

	public boolean isShouldBeTrue() {
		return shouldBeTrue;
	}

	public BunchOfAnnotationObject setShouldBeTrue(boolean shouldBeTrue) {
		this.shouldBeTrue = shouldBeTrue;
		return this;
	}

	public boolean isShouldBeFalse() {
		return shouldBeFalse;
	}

	public BunchOfAnnotationObject setShouldBeFalse(boolean shouldBeFalse) {
		this.shouldBeFalse = shouldBeFalse;
		return this;
	}

	public int getMinMax() {
		return minMax;
	}

	public BunchOfAnnotationObject setMinMax(int minMax) {
		this.minMax = minMax;
		return this;
	}

}
