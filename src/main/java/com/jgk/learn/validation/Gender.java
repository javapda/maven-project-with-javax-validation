package com.jgk.learn.validation;

public enum Gender {
	MALE, FEMALE, OTHER;
}
