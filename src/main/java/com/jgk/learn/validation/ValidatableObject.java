package com.jgk.learn.validation;

import javax.validation.constraints.NotNull;
/**
 * https://docs.jboss.org/hibernate/validator/4.0.1/reference/en/html/validator-usingvalidator.html
 * @author jkroub
 *
 */
public class ValidatableObject {
	
	@NotNull(message="howdy")
	String name;
	
	public ValidatableObject(String name2) {
		name = name2;
	}

}
